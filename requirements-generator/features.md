# 🔘 Features

<figure><img src="../.gitbook/assets/image (66).png" alt=""><figcaption></figcaption></figure>

*   Generate security requirements based on your services functionality.

    * **Cloud** version —[https://requirements.whitespots.io/en](https://requirements.whitespots.io/en)
    * **On-demand** version —[https://github.com/Whitespots-OU/security-requirements-generator](https://github.com/Whitespots-OU/security-requirements-generator)

    For example, you can edit the **button title** and the link to **contact security team**. 😄
