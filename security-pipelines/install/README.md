# 📥 Install

### System Requirements for Portal usage:

**Sourse Code Management System (SCMS) - GitLab or GitHub**:

* Minimum system resources: 8 GB of RAM and 4 CPU cores. &#x20;
* Free disk space for installation and storage of SCMS and related data.&#x20;
* Network access for external users (users must be able to connect to GitLab over the network).

**Connectivity between system components**:

* Ensure access from the SCMS server to the server with GitLab Runner.&#x20;
* Ensure access from the SCMS Runner server to the server with the portal.

### Instalation

* [**GitHub usage**](github-ci.md)
* [**GitLab usage**](gitlab-ci.md)
