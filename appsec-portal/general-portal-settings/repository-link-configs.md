# Repository Link Configs

{% hint style="info" %}
<img src="../../.gitbook/assets/image (8).png" alt="" data-size="line">Used in working with an [**Auditor**](broken-reference)
{% endhint %}

This configuration enables the Portal to provide a link to the repository sources using the provided finding path and line. Associate it with Product Assets of the Repository type in [product settings](../../auditor/settings/appsec-portal-cooperation/product-asset-setting.md).

Navigate to **Settings → Integrations → Repository Link** to specify pattern for repository link

<figure><img src="../../.gitbook/assets/repos link.png" alt=""><figcaption></figcaption></figure>

To help you get started, here is the _URL builder_:

<figure><img src="../../.gitbook/assets/repos link2.png" alt=""><figcaption></figcaption></figure>

* select the URL pattern of your Git hosting service: choose from the **Example patterns** provided (available for GitHub, GitLab and Bitbucket) or create a new one in **Pattern** field;
* select a protocol shema;
* **Keywords definition** will help you build correct way to project repository, you can also hover your mouse over the highlighted sections of the URL in the **Result** box to see what they mean

<figure><img src="../../.gitbook/assets/image (9).png" alt=""><figcaption></figcaption></figure>
