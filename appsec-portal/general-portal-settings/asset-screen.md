# Asset screen

The Assets screen allows you to view, modify, delete or add assets to your products.

To access the Assets screen, select **Assets** from the left hand navigation bar.

<figure><img src="../../.gitbook/assets/asset screen 1.png" alt=""><figcaption></figcaption></figure>

**Find** the asset you need in the list of the appropriate group (Repository, Docker Image, Domain, or Host), by using Filter or by using the search option.

<figure><img src="../../.gitbook/assets/asset screen 6.gif" alt=""><figcaption></figcaption></figure>

**Modify** or **Delete** an asset you need by clicking the three dots to the right of it in the list and selecting the **Edit** or **Delete** function.

<figure><img src="../../.gitbook/assets/asset screen 4.gif" alt=""><figcaption></figcaption></figure>

**Add** an Asset to the appropriate Group (Repository, Docker Image, Domain, or Host) using the Add button in the right pane. You can also select a group in the window that opens. To see how to set up an Asset, click [**here**](product-settings/product-asset.md).

<figure><img src="../../.gitbook/assets/asset screen 5.gif" alt=""><figcaption></figcaption></figure>
