---
description: >-
  Find vulnerabilities, misconfigurations, secrets, SBOM in containers,
  Kubernetes, code repositories, clouds and more
---

# Trivy

Trivy is a versatile security scanning tool designed to identify potential vulnerabilities in both **container images** and **code repositories** (two operating modes). It offers comprehensive coverage of potential security issues, including known vulnerabilities in operating system packages and application dependencies.

Trivy's container scanning capabilities are particularly noteworthy, as it can inspect Docker images for vulnerabilities within OS packages, libraries, and other components. This ensures that containerized applications are built on a secure foundation, minimizing the risk of exploitation through known vulnerabilities.

In addition to container scanning, Trivy also supports code scanning by examining code repositories for security issues.
