---
description: to check your source code for known vulnerabilities
---

# GitLab SAST Report

Find Security Bugs Scan based on [GitLab SAST](https://docs.gitlab.com/ee/user/application\_security/sast/) Report is a specialized tool designed to perform static analysis of application source code, aiming to identify potential vulnerabilities and security issues. It scans the codebase for common issues such as SQL injections, cross-site scripting (XSS), and other security vulnerabilities.

Find Security Bugs Scan analyzes the code by examining its structure and logic, looking for patterns and code constructs that may indicate security weaknesses.
