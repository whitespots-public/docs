# MobSFScan

**Auditor Job Name**: CodeQL Scan (SARIF)\
**AppSec Portal Importer Name**: Mobsfscan

[MobSFScan ](https://github.com/MobSF/mobsfscan)is a static analysis tool that can find insecure code patterns in your **Android** and **iOS** source code. Supports Java, Kotlin, Swift, and Objective C Code. mobsfscan uses MobSF static analysis rules and is powered by semgrep and libsast pattern matcher.
