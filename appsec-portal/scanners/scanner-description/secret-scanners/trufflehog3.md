---
description: >-
  Trufflehog3 is a popular open-source tool for detecting secrets and
  credentials in source code repositories.
---

# Trufflehog3

[Trufflehog3](https://github.com/feeltheajf/trufflehog3) uses _regular expressions_ to scan for patterns that match common formats for **secret keys**, such as **AWS keys**, **private keys**, and other sensitive data. It can also detect **secrets that have been obfuscated** or **encoded** in various ways, making it a valuable tool for detecting even well-hidden secrets.

While Trufflehog3 is primarily used for detecting secrets and credentials, it can also be used to search for other types of sensitive data, such as **Personally Identifiable Information (PII)**. This makes it a versatile tool for ensuring the security and privacy of your codebase.
