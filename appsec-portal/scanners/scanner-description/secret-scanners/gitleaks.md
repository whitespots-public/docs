---
description: >-
  Gitleaks is a powerful open-source tool that helps you find and eliminate
  sensitive information leaks in your Git repositories.
---

# Gitleaks

[Gitleaks](https://github.com/gitleaks/gitleaks) uses _regular expressions_ to search for **specific patterns** of sensitive information. By default, it comes with a list of regular expressions that cover **common secrets**, but it can also be customized to match specific patterns.

One of the unique features of Gitleaks is its ability to scan not only the repository itself but also its **entire commit history**, making it a powerful tool for detecting information leaks that may have been committed in the past.
