---
description: This helps you discover vulnerabilities in web applications
---

# OWASP Zap

[GitLab OWASP Zap](https://docs.gitlab.com/ee/user/application\_security/dast/proxy-based.html) is a penetration testing and vulnerability detection tool for **web applications**. It offers capabilities to scan web applications for vulnerabilities like **SQL injection**, **cross-site scripting (XSS)**, and more. OWASP Zap helps developers explore web application security and safeguard against known attacks.
