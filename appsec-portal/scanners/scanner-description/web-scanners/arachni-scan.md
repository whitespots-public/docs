---
description: >-
  Arachni Scan is a security scanner designed to identify vulnerabilities and
  security issues in web applications.
---

# Arachni Scan

It utilizes a combination of black-box scanning techniques and a comprehensive set of built-in checks to assess the security posture of web applications.

One of the notable features of Arachni is its ability to crawl and scan web applications in a way that **mimics the behavior of a real user**. This allows the scanner to detect vulnerabilities that would not be visible through a standard scan. Arachni also has the ability to identify vulnerabilities in various technologies and programming languages including **PHP**, **Ruby on Rails**, and **Java**.
