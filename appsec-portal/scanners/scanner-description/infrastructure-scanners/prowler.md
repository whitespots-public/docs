---
description: Open Source security tool to perform AWS, GCP and Azure security
---

# Prowler

[**Prowler**](https://docs.prowler.pro/en/latest/) is a security scanning tool designed for assessing security and identifying potential vulnerabilities within **Amazon Web Services** (AWS) infrastructure and resources. This tool is developed to assist organizations and security engineers in detecting and addressing threats and vulnerabilities in their AWS environment.&#x20;

Prowler enables the scanning of various types of AWS resources, including virtual machines, data storage, databases, and more.
