---
description: >-
  Nuclei is an open-source project that enables automated detection and
  exploitation of vulnerabilities in web applications.
---

# Nuclei

It supports a variety of protocols, including **HTTP**, **DNS**, and **FTP**, and allows users to create **custom templates** for scanning.

One interesting feature of [Nuclei](https://github.com/projectdiscovery/nuclei) is its ability to automatically correlate multiple vulnerabilities into a single issue. This can save time and effort for security teams, as it eliminates the need to manually review and correlate multiple vulnerabilities across different scans.
