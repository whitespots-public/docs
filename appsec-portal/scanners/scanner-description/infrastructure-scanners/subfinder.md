---
description: Fast passive subdomain enumeration tool
---

# Subfinder

[Subfinder ](https://github.com/projectdiscovery/subfinder)is a specialized subdomain discovery tool used to identify **subdomains** associated with a target domain or web application. It assists in gathering critical information during enumeration phases of security assessments and penetration testing.

Subfinder scans domain names and web applications, searching for subdomains that may be related to the target. By doing so, it helps security professionals and penetration testers to create a more comprehensive picture of the attack surface and potential entry points for security assessments.
