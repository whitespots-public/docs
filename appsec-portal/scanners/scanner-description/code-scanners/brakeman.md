# Brakeman

[Brakeman ](https://brakemanscanner.org/)is a static security vulnerability analyzer for applications developed in Ruby on Rails. It analyzes the application source code for potential vulnerabilities without actually executing the application.

{% hint style="info" %}
Currently the scanner is only supported in the **Auditor**
{% endhint %}
