---
description: >-
  Bandit is a popular open-source tool that scans Python code for security
  vulnerabilities.
---

# Bandit

[Bandit scaner](https://github.com/PyCQA/bandit) and [Bandit scaner (GitLab)](https://gitlab.com/gitlab-org/security-products/analyzers/bandit) has a _wide range of plugins_ to detect various types of security vulnerabilities, including **SQL injection**, **cross-site scripting**, and **hardcoded passwords**. It can also detect insecure use of cryptography, such as **weak encryption algorithms** or **incorrect usage of hash functions**.
