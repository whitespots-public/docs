---
description: scanner detecting the use of JavaScript libraries with known vulnerabilities
---

# Retire.js

[Retire.js](https://github.com/RetireJS/retire.js) is a specialized tool designed to analyze **JavaScript** code for deprecated and vulnerable libraries and dependencies. It focuses on identifying outdated or known vulnerable components within JavaScript code, contributing to the enhancement of web application security.
