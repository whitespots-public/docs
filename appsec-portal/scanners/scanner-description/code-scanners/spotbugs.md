# SpotBugs

[SpotBugs](https://spotbugs.github.io/) analyzes **Java** source code for possible problems in security, efficiency, and programming style. It is based on the FindBugs project, but provides additional functionality and improved bug detection capabilities.

{% hint style="info" %}
Currently the scanner is only supported in the **Auditor**
{% endhint %}
