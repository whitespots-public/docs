---
description: >-
  Terrascan is an open-source tool that is used to detect compliance and
  security violations across Infrastructure as Code (IaC) frameworks.
---

# Terrascan

[Terrascan](https://github.com/tenable/terrascan) ensures that IaC definitions adhere to security best practices and can detect issues that may cause security breaches, data loss, or service disruptions. Terrascan supports several popular IaC frameworks, including **Terraform**, **Kubernetes**, **Helm**, **AWS CloudFormation**, **Azure Resource Manager**, and **Google Cloud Deployment Manager**.

The tool uses a set of **predefined policies** that can be customized to match the organization's specific security and compliance requirements. The policies are based on industry-standard security frameworks such as **NIST**, **CIS**, **PCI-DSS**, and **GDPR**.
