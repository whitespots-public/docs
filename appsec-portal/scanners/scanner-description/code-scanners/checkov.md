---
description: >-
  Checkov Scan is an open-source static analysis tool that scans
  infrastructure-as-code (IaC) files to identify potential security risks and
  compliance violations.
---

# Checkov

[Checkov](https://github.com/bridgecrewio/checkov) supports various IaC formats, including **Terraform**, **Kubernetes**, **AWS** **CloudFormation**, and **Azure Resource Manager** templates. Checkov can be used to identify misconfigurations and enforce compliance with industry-standard policies and regulations.

Checkov Scan works by analyzing the code structure and applying a set of built-in and custom rules. The rules are written in YAML format and can be easily extended or modified to fit specific use cases. Each rule checks for a specific condition, such as the use of insecure protocols or the exposure of sensitive data, and generates a report with the findings. The report can be exported in various formats, such as JSON, HTML, or JUnit.
