# PHP\_CodeSniffer

**Auditor Job Name**: Phpcs, Gitlab Php\
**AppSec Portal Importer Name**: GitLab Semgrep, PHP Security Audit v2

Scanner tokenizes PHP files and detects violations of a defined set of coding standards.

PHP\_CodeSniffer is a set of two PHP scripts; the main `phpcs` script that tokenizes PHP, JavaScript and CSS files to detect violations of a defined coding standard, and a second `phpcbf` script to automatically correct coding standard violations. PHP\_CodeSniffer is an essential development tool that ensures your code remains clean and consistent.
