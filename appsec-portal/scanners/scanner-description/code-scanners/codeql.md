---
description: >-
  CodeQL is a powerful static analysis tool for analyzing and finding security
  vulnerabilities in code.
---

# CodeQL

CodeQL uses a semantic code analysis engine to understand the behavior of code and find potential security issues, including **SQL injection**, **cross-site scripting**, and more. CodeQL can be used to analyze code written in a wide range of programming languages, including **C++**, **Java**, **Python**, and **JavaScript**, making it a versatile tool for any development team.

CodeQL works by creating a **graph database** that models the behavior of code. This database allows CodeQL to understand how different parts of the code interact with each other and identify potential security issues that may arise from those interactions. CodeQL also provides a range of powerful query languages that allow developers to write custom queries to find specific security issues or patterns in their code.
