---
description: >-
  Semgrep is a fast, open-source tool that scans source code to find programming
  errors, security vulnerabilities, and policy violations.
---

# Semgrep

**Auditor Job Name**: Gitlab Semgrep\
**AppSec Portal Importer Name**: [GitLab Semgrep](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep), Semgrep JSON Report

[Semgrep](https://github.com/semgrep/semgrep) supports several programming languages such as:

* Python
* JavaScript
* Java
* Go
* Ruby
* TypeScript
* C#
* Kotlin
* PHP
* Swift

The Semgrep can be used to scan for security issues such as:

* SQL injection
* Cross-site scripting (XSS)
* Command injection
* Authentication and authorization issues
* Insecure cryptography
* Code injection
* Path traversal
* File inclusion
* Information leakage
* XML external entity injection (XXE)
* Server-side request forgery (SSRF)
* and more

One interesting feature of Semgrep is its ability to detect security issues in **complex codebases**. It uses a powerful pattern-matching engine to identify vulnerabilities and is highly customizable.
