---
description: GitLab Analyzer for Infrastructure as Code (IaC) projects
---

# KICS

[GitLab KICS](https://gitlab.com/gitlab-org/security-products/analyzers/kics) (Keeping Infrastructure as Code Secure) is a tool for identifying security vulnerabilities and policy violations in **infrastructure code** (IaC). It supports various IaC formats, such as **Terraform**, **Kubernetes**, **AWS CloudFormation**, and **Azure Resource Manager templates**. KICS helps identify issues in infrastructure code, enabling developers and operations teams to mitigate potential risks.
