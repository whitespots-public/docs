---
description: Dockerfile linter, validate inline bash, written in Haskell
---

# Hadolint

[Hadolint ](https://github.com/hadolint/hadolint)is a specialized **Dockerfile** linter designed to ensure the correctness of Dockerfile syntax, adherence to best practices, and the identification of potential issues related to Docker image creation. It focuses on code quality and conformity to Dockerfile standards, assisting in the creation of secure and well-structured Docker images.

Hadolint analyzes Dockerfiles by checking for common mistakes, adherence to Dockerfile guidelines, and potential security issues. This tool can help developers and DevOps teams maintain high-quality Dockerfiles, ensuring that the resulting images are secure, efficient, and error-free.

One notable feature of Hadolint is its ability to provide feedback on Dockerfiles even before the images are built, allowing developers to catch potential issues early in the development process.
