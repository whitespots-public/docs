---
description: >-
  ESLint is a popular open-source static analysis tool that is used to find and
  fix problems in JavaScript code.
---

# ESLint

[ESLint scaner](https://github.com/eslint/eslint) and [ESLint scaner(GitLab) ](https://gitlab.com/gitlab-org/security-products/analyzers/eslint)checks code for **common errors** and **coding style issues**, ensuring that the code is consistent and maintainable.

ESLint can detect a wide variety of issues, from simple syntax errors to more complex issues like **security vulnerabilities**. It supports a range of configurations that can be customized to suit your specific needs.
