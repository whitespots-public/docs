---
description: Dependency Scanning analyzer that uses the GitLab Advisory Database
---

# Gemnasium

[GitLab Gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) is a tool for automatically detecting vulnerabilities in project dependencies. It scans the used **libraries** and **components** to identify known vulnerabilities, providing developers with information about the need to update or replace dependencies to mitigate security risks.
