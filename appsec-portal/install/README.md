---
description: Welcome to the installation guide for AppSec Portal!
---

# 📥 Install

## Installation

There are **three steps** to installing the AppSec Portal:&#x20;

* [Obtaining a license](license-obtaining.md)
* [Installing the application](installation.md)
* [Entering the license key in the installed application](get-started-with-the-appsec-portal/)

## Update

See how [**to update**](update.md) your AppSec Portal

## API endpoints

View the [endpoints](accessing-the-appsec-portal-api-endpoints.md) for API connections

## Database transfer

[Migrate your data](./#database-transfer) when you move to a new host



##
