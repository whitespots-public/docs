# Available bulk actions

You can set bulk for your findings:

* severity type;
* status setting: **Reject or Resolve** for Verified and Unverified findings; \
  **Reopen** for Rejected and Resolved findings;
* deleting findings;
* adding or deleting your own custom tags;
* &#x20;adding or deleting groups;
* link issue - link findings to another finding in Jira

<figure><img src="../../.gitbook/assets/find view7.gif" alt=""><figcaption><p>set severity</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/find view8.gif" alt=""><figcaption><p>set status</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/find view9.gif" alt=""><figcaption><p>delete findings</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/tag(1).gif" alt=""><figcaption><p>add/delete tag</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/group(1).gif" alt=""><figcaption><p>add/delete group</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/link.gif" alt=""><figcaption><p>link issue</p></figcaption></figure>
