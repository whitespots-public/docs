# All findings view

<figure><img src="../../.gitbook/assets/find veiw2.png" alt=""><figcaption></figcaption></figure>

Every finding can be viewed, edited and verified **on the same page** one by one:

<figure><img src="../../.gitbook/assets/find view3.gif" alt=""><figcaption></figcaption></figure>

Or simply click the checkbox to [**bulk edit**](available-bulk-actions.md) the whole findings:

<figure><img src="../../.gitbook/assets/find view5.gif" alt=""><figcaption></figcaption></figure>
