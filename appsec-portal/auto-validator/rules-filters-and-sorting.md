# Rules filters & sorting

Use the handy **filters** to find the rules you need at a glance:

<figure><img src="../../.gitbook/assets/autoval5(1).gif" alt=""><figcaption></figcaption></figure>

Search rules by **value** name:

<figure><img src="../../.gitbook/assets/autovalid6.gif" alt=""><figcaption></figcaption></figure>
