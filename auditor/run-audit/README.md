# 🚀 Run Audit

You can run the Auditor through the Appsec Portal

* [Manually](appsec-portal-cooperation/run-audit-manually.md)
* [By setting up a schedule](appsec-portal-cooperation/scheduled-audit-run.md)

Faindings detected by the Auditor will be [displayed](../../appsec-portal/findings-view/) in the Appsec portal

{% hint style="info" %}
If findings are detected during scanning, they will be automatically submitted to the Appsec portal when **all** scanners in the sequence have completed their work
{% endhint %}
