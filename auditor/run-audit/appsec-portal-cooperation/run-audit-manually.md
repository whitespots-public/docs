# Run Audit Manually

1. Сlick on **Audit** on the home page

<figure><img src="../../../.gitbook/assets/run audit 1.png" alt=""><figcaption></figcaption></figure>

2. From the drop-down list:&#x20;

* under **Scanner sequence** choose the pipeline with the desired scanner sequence
* under **Products** select the product or products to be scanned

3. Enter the **branch name** (optional)
4. Select or [create ](../../../appsec-portal/general-portal-settings/product-settings/product-asset.md)**asset**(-s) in which scanning will be performed. \
   By default, all product-specific assets are selected
5. Click **Run Audit**

<figure><img src="../../../.gitbook/assets/run audit(1).gif" alt=""><figcaption></figcaption></figure>

5. Optionally to navigate to the **Auditor Pipelines page**. Here, you can monitor the progress of the scan or choose to **Close** the window.

<figure><img src="../../../.gitbook/assets/run audit 3.png" alt=""><figcaption></figcaption></figure>
