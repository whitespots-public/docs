# AppSec Portal cooperation

If you use a AppSec Portal, you can set up the Auditor directly on the portal

**Auditor Settings:**

1. [**Product Asset**](broken-reference)**:**
   * Enter information about the location of your product.
2. [**Auditor Configurator**](auditor-configurator.md)**:**
   * Set up the configurator by providing credentials for connecting to your resources.
3. [**Jobs**](../jobs/)**:**
   * Here you can find the scanners in use along with standard commands and variables for each. If needed, you can [modify](../jobs/job-configuration.md) or add to these parameters.
4. [**Sequences**](sequences/) **(Pipelines):**
   * Use sequences to define the order of scanner operations.&#x20;
