---
description: Welcome to the installation guide for Auditor!
---

# 📥 Install

## Installation

See how[ **install** ](installation.md)Auditor

## Update

See how [**update**](update.md) Auditor
